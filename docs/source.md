# `redhat/source`

Configure a job to build a source RPM (`.src.rpm`) for this project.

## Description

This component creates multiple jobs -- one for each chosen RedHat version --
each with a common prefix, that take in a pre-existing upstream source
distribution (tarball), and use
[`rpmbuild`](https://www.redhat.com/sysadmin/create-rpm-package)
to create a source RPM.

After each build `rpmlint` is used to scan the new RPMs to produce a GitLab
[Code Quality report](https://git.ligo.org/help/ci/testing/code_quality.html).

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/redhat/source@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `cache_dir` | `".cache/rpm"` {: .nowrap } | The path to cache downloaded RPMs to (relative to `$CI_PROJECT_DIR`) |
| `disable_repos` {: .nowrap } | `""` | Space-separate list of RPM repo names to disable |
| `dnf_update` | `true` | Update all installed packages with `dnf update` before proceeeding |
| `enable_repos` | `""` | Space-separate list of RPM repo names to enable |
| `epel` | `false` | Pre-configure the EPEL (Extra Packages for Enterprise Linux) repository |
| `job_name` | `"redhat_source"` {: .nowrap } | Name to give this job |
| `needs` | | List of jobs whose artifacts are needed for the source RPM build |
| `dynamic_version` | `true` | Automatically detect package version from Python `PKG-INFO` metadata file and update RPM `.spec` file |
| `redhat_versions` | `[8]` | Versions of Red Hat to build for |
| `rpmbuild_options` | `""` | Options to pass to `rpmbuild` |
| `rpmlint_options` | `""` | Options to pass to `rpmlint` |
| `source_distribution_name` {: .nowrap } | `"*.tar.*"` | Name of the source distribution file to build from |
| `stage` | `"build"` | The stage to add jobs to |

## Notes

### Requires an upstream source distribution

Creating a source RPM is only supported when starting from an
upstream tarball that contains an RPM spec (`.spec`) file.

This must be configured independently of this component and the tarball
provided to the `redhat/source` component via the `needs` input.
See [_Examples_](#examples) for an example.

## Customisation

### Rpmlint { # rpmlint }

The output of `rpmlint` can be customised by creating a file `.rpmlint.toml`
in the root of the git project that augments the default configuration.

See <https://github.com/rpm-software-management/rpmlint#configuration> for
some details on the configuration format.

If you wish to choose a path other than `.rpmlint.toml` for the lintian
configuration file, pass that in `rpmlint_options`:

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/redhat/source@<VERSION>
    inputs:
      rpmlint_options: "---config myrpmlint_config.toml"
```

## Examples

### Build a source RPM for a Python project {: #python }

!!! example "Create a source RPM for a Python project"

    ```yaml
    include:
      # generate a source distribution
      - component: git.ligo.org/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
          job_name: sdist
      # generate a source RPM
      - component: git.ligo.org/computing/gitlab/components/redhat/source@<VERSION>
        inputs:
          needs: [sdist]
    ```
