# `redhat/test`

Configure jobs to test newly built RPMs.

## Description

This component creates multiple jobs, each with a common prefix, that
take in newly built binary RPMs (downloaded as artifacts of other jobs
in the pipeline), bundle them into a local Yum repository, and install
them before executing the user-specific test commands.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/redhat/test@<VERSION>
    inputs:
      test_script:
        # sanity check executable installed from RPM
        - /usr/bin/my-script --help
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `build_prefix` | `redhat_build` {: .nowrap } | Job name prefix used for build jobs on which to depend. |
| `cache_dir` | `".cache/rpm"` {: .nowrap } | The path to cache downloaded RPMs to (relative to `$CI_PROJECT_DIR`) |
| `disable_repos` {: .nowrap } | `""` | Space-separate list of RPM repo names to disable |
| `dnf_update` | `true` | Update all installed packages with `dnf update` before proceeeding |
| `enable_repos` | `""` | Space-separate list of RPM repo names to enable |
| `epel` | `false` | Pre-configure the EPEL (Extra Packages for Enterprise Linux) repository |
| `git_strategy` | `fetch` | Value for [`GIT_STRATEGY`](https://git.ligo.org/help/ci/runners/configure_runners.html#git-strategy). |
| `job_prefix` | `redhat_test` | Prefix to use for job name. |
| `redhat_versions` {: .nowrap } | `[8]` | RedHat versions to test on. |
| `stage` | `test` | The stage to add jobs to |
| `test_install` | `""` | Extra packages to install to support the `test_script`. |
| `test_script` | | Script commands to run as part of the test jobs. |

## Notes

### `redhat/test` jobs require matching jobs from `redhat/build`

`redhat/test` jobs are automatically configured with `needs` referencing
the job from [`redhat/build`](./build.md) with the same `redhat_version` value.
It is required that when configuring the `redhat/test` you also configure the
`redhat/build` component.

See [_Examples_](#examples) for examples.

## Customisation

All customisation should be achieved through the [`inputs`](#inputs),
or by overriding the test job template.

See [_Customimse the test job template_](#test) under [_Examples_](#examples)
below for more details.

## Examples

### Build and test a Python project {: #python }

!!! example "Build and test a Python application on RedHat"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
      - component: git.ligo.org/computing/gitlab/components/redhat/build@<VERSION>
        inputs:
          needs: [sdist]
      - component: git.ligo.org/computing/gitlab/components/redhat/test@<VERSION>
        inputs:
          test_install: python3-pytest
          test_script:
            - python3 -m pytest --pyargs my_library.tests
    ```

### Customise the test job template {: #test }

!!! example "Customise the test job template"

    This example demonstrates how to heavily customise the `redhat_test` job
    template used for testing of all `redhat_versions`, including adding
    custom artifacts for test and coverage reporting for a Python project.

    ```
    include:
      - component: git.ligo.org/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
      - component: git.ligo.org/computing/gitlab/components/redhat/build@<VERSION>
        inputs:
          needs: [sdist]
      - component: git.ligo.org/computing/gitlab/components/redhat/test@<VERSION>

    # customise the template used for the version-specific test jobs
    redhat_test:
      variables:
        COVERAGE_FILE: ".coverage-${CI_JOB_NAME_SLUG}"
      script:
        - dnf -y install python3 python3-pytest python3-pytest-cov
        - python3 -m pytest --pyargs my_library.tests --cov my_library
        - python3 -m coverage xml
      artifacts:
        reports:
          coverage_report:
            coverage_format: cobertura
            path: coverage.yml
          junit: junit.xml
        paths:
          - .coverage*
    ```

    The customisations are
    [merged](https://git.ligo.org/help/ci/yaml/yaml_optimization.html#merge-details)
    with the template configured by the `computing/gitlab/components/redhat/test`
    component, so anything that isn't specified manually is preserved from the
    template.
