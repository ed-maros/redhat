# -- Test a newly build RedHat binary packages

spec:
  inputs:

    # -- common inputs

    cache_dir:
      default: ".cache/rpm"
      description: "The path to cache downloaded RPMs to (relative to CI_PROJECT_DIR)"
    disable_repos:
      default: ""
      description: "Space-separate list of RPM repo names to disable"
      type: string
    dnf_update:
      default: true
      description: "Update all packages with 'dnf update' before proceeding"
      type: boolean
    enable_repos:
      default: ""
      description: "Space-separate list of RPM repo names to enable"
      type: string
    epel:
      default: false
      description: "Pre-configure the EPEL (Extra Packages for Enterprise Linux) repository"
      type: boolean

    # -- test inputs

    stage:
      default: test
      description: "The stage to add jobs to"
      type: string
    redhat_versions:
      default:
        - 8
      description: "Version (number) of Red Hat to build for"
      type: array
    job_prefix:
      default: redhat_test
      description: "Prefix to use for build job name"
      type: string
    build_prefix:
      default: redhat_build
      description: "Job name prefix used for build jobs on which to depend"
      type: string
    git_strategy:
      default: fetch
      description: "Value for GIT_STRATEGY"
      type: string
    test_install:
      default: ""
      description: "Extra packages to install to support the test_script"
      type: string
    test_script:
      default:
        - |
          # no script command passed, error
          echo -e "\x1B[91mError: no inputs:test_script passed to include, or script commands provided as an override to the redhat/test job template\x1B[0m"
          exit 1
      description: "Script commands to run as part of the test job"
      type: array

---

include:
  - local: templates/base.yml
    inputs:
      cache_dir: $[[ inputs.cache_dir ]]
      disable_repos: $[[ inputs.disable_repos ]]
      dnf_update: $[[ inputs.dnf_update ]]
      enable_repos: $[[ inputs.enable_repos ]]
      epel: $[[ inputs.epel ]]

# template that allows for easy augmentation of test script
$[[ inputs.job_prefix ]]:
  rules:
    # this is just a template
    - when: never
  extends: .redhat_base
  stage: $[[ inputs.stage ]]
  image: igwn/base:el$REDHAT_VERSION-testing
  variables:
    COVERAGE_FILE: "$CI_PROJECT_DIR/.coverage-${CI_JOB_NAME_SLUG}"
    GIT_STRATEGY: "$[[ inputs.git_strategy ]]"
  before_script:
    # set up DNF
    - !reference [.redhat_base, before_script]
    # setup local Yum repository
    - dnf install -y -q
        createrepo
        "dnf-command(repository-packages)"
        dnf-utils
        rpmlint
    - LOCAL_REPO="${CI_PROJECT_DIR}/local-builds"
    - mkdir -p ${LOCAL_REPO}
    - rm -fv *.src.rpm
    - mv -v *.rpm ${LOCAL_REPO}/
    - createrepo --quiet "${LOCAL_REPO}"
    - |
      cat > /etc/yum.repos.d/local-builds.repo <<EOF
      [local-builds]
      name=Local builds
      baseurl=file://${LOCAL_REPO}
      enabled=1
      gpgcheck=0
      priority=1
      EOF
    - dnf repository-packages local-builds list
    # find all local packages and install them
    - LOCAL_PACKAGES=$(repoquery -a --repoid=local-builds --qf=%{name})
    - dnf info ${LOCAL_PACKAGES}
    - dnf install -y
        $[[ inputs.test_install ]]
        ${LOCAL_PACKAGES}
    # lint the installed RPMs (but don't fail)
    - rpmlint ${LOCAL_PACKAGES} || true
  script: $[[ inputs.test_script ]]

# -- version-specific non-abstract build jobs
# ideally this would be served by a parallel:matrix,
# but we can't use parallel:matrix with needs properly,
# so we just build out separate jobs

$[[ inputs.job_prefix ]]_el7:
  extends: $[[ inputs.job_prefix ]]
  rules:
    - if: "$CI_COMMIT_BRANCH && '$[[ inputs.redhat_versions ]]' =~ /(el)?7/"
      variables:
        REDHAT_VERSION: 7
  needs: ["$[[ inputs.build_prefix ]]_el7"]

$[[ inputs.job_prefix ]]_el8:
  extends: $[[ inputs.job_prefix ]]
  rules:
    - if: "$CI_COMMIT_BRANCH && '$[[ inputs.redhat_versions ]]' =~ /(el)?8/"
      variables:
        REDHAT_VERSION: 8
  needs: ["$[[ inputs.build_prefix ]]_el8"]

$[[ inputs.job_prefix ]]_el9:
  extends: $[[ inputs.job_prefix ]]
  rules:
    - if: "$CI_COMMIT_BRANCH && '$[[ inputs.redhat_versions ]]' =~ /(el)?9/"
      variables:
        REDHAT_VERSION: 9
  needs: ["$[[ inputs.build_prefix ]]_el9"]

$[[ inputs.job_prefix ]]_el10:
  extends: $[[ inputs.job_prefix ]]
  rules:
    - if: "$CI_COMMIT_BRANCH && '$[[ inputs.redhat_versions ]]' =~ /(el)?10/"
      variables:
        REDHAT_VERSION: 10
  needs: ["$[[ inputs.build_prefix ]]_el10"]
