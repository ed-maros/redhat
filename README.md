# CI/CD components for Red Hat

This project provides [CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
for a software project that includes Red Hat build, test, and quality checking.

[[_TOC_]]

## Usage

You can add the individual component templates to an existing `.gitlab-ci.yml`
file by using the `include:` keyword, where `<VERSION>` is the latest released
tag.

See the
[`example/`](https://git.ligo.org/computing/cicd-components/redhat/-/tree/main/example/)
project for a demonstration of how this component set may be used in a real
project.

### Components

## Contributing

Please read about CI/CD components and best practices at: <https://git.ligo.org/help/ci/components/index.html>.

All interactions related to this project should follow the
[LIGO-Virgo-KAGRA Code of Conduct](https://dcc.ligo.org/LIGO-M1900037).

For more details on contributing to this project, see `CONTRIBUTING.md`.
